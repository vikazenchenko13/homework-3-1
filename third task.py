def decorator_fib(func):
	def dec(*args):
		x = func(*args)
		return filter(lambda y: y % 2 == 0, x)
	return dec


@decorator_fib
def generator(count):
	first, second = 0, 1
	while first < count:
		yield first
		first, second = second, first + second


y = int(input("Enter number of digits: "))
for i in generator(y):
	print(i)
