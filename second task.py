import random

n = int(input("Enter the length of the list: "))
numbers = random.sample(range(500), n)
print(numbers)
odd_numbers = filter(lambda x: (x % 2 != 0), numbers)
square_numbers = list(map(lambda x: x ** 2, odd_numbers))
print(square_numbers)
